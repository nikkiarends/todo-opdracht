<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreItem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:20',
            'description' => 'required|max:200',
        ];
    }

    public function messages()
    {
        return [
            "title.required" => "Vul een titel in",
            "title.max" => "De ingevoerde titel is te lang",
            "description.required" => "Vul een omschrijving in",
            "description.max" => "De ingevoerde beschrijving is te lang",
        ];
    }
}
