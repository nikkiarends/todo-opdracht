import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import store from './store'
import router from './router'

Vue.prototype.$axios = axios

new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
})
