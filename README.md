
> An example SPA project for the basics of a webstore/item-list app. Basic functions (adding a new item, deleting an item, editing an item) are deployed via vuex store. 
> Packages used:

- Vue
- Vuex
- Vue router
- Laravel
- Bootstrap

## Build Setup

``` bash
# install dependencies
npm install
composer install

# generate files
create .env file based on .env.example
php artisan key:generate to generate app key

#seed table
#create a database to seed into
php artisan migrate:fresh --seed

# serve with hot reload at localhost:8080
npm run dev
php artisan serve

# build for production with minification
npm run build
```
For any questions about Laravel, visit documentation:  https://laravel.com/docs/5.8

